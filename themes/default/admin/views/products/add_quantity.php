<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <?php echo admin_form_open('products/add_quantity/'.$product_id, 'data-toggle="validator"'); ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> <?php echo ($product_data !== FALSE && $product_data->has_multiple_raw == 1) ? 'Add quantity for '.$product_data->name : 'Add Quantity'; ?></h4>
        </div>

        <div class="modal-body">
            <?php if ($product_data !== FALSE): ?>

                <?php if ($product_data->has_multiple_raw == 1): ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <label>Date*</label>
                            <?= form_input($date, '', 'class="form-control datetime" placeholder="Date" required="required" autocomplete="off"'); ?>
                        </div>

                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label>Store Quantity*</label>
                                <div class="well well-sm">
                                    <div class="row">
                                        <?php if ($warehouses !== FALSE): ?>
                                            <?php foreach ($warehouses as $warehouse): ?>
                                                <div class="col-xs-12 col-md-6">
                                                    <label><?php echo $warehouse->name; ?></label>
                                                    <input type="hidden" name="warehouse_ids[]" value="<?= $warehouse->id; ?>">
                                                    <input type="text" name="qty[]" class="form-control" placeholder="Quantity" required="required">
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <p>This feature is only for <strong>products with multiple raw products</strong></p>
                <?php endif; ?>

            <?php else: ?>
                <p>This product no longer exists.</p>
            <?php endif; ?>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <?php if ($product_data !== FALSE && $product_data->has_multiple_raw == 1): ?>
                <button type="submit" class="btn btn-primary">Submit</button>
            <?php endif; ?>
        </div>
        <?php form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('form[data-toggle="validator"]').bootstrapValidator({ excluded: [':disabled'] });
        jQuery('#date').datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });
</script>