<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Subscription Expired</title>
    <script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<?=admin_url()?>'; }</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $assets ?>images/posit-icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/green.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/helpers/login.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="login-page">
    <div class="page-back bgwhite">
        <div class="text-center">
            <?php
                echo '<img src="' . base_url('assets/uploads/logos/logo3.png') . '" alt="POSit Cloud" style="margin-bottom:10px; width: 300px;" />';
            ?>
        </div>

        <div id="login">
            <div class="container text-center">
                <h3 class="text-danger">Your subscription to POSit Cloud has expired.</h3>
                <p>
                    <?php
                        $crash_date = new DateTime($Settings->expiry_date);
                        $crash_date->modify('+14 days');
                    ?>
                    Please renew your account before <strong><?php echo $crash_date->format('F j, Y') ?></strong> to avoid deletion of data.
                </p>
                <br />
                <a class="btn btn-primary btn-lg" href="<?php echo admin_url('subscription/auto_renew'); ?>">Try to Auto Renew</a>
            </div>
        </div>
    </div>

    <script src="<?= $assets ?>js/jquery.js"></script>
    <script src="<?= $assets ?>js/bootstrap.min.js"></script>
    <script src="<?= $assets ?>js/jquery.cookie.js"></script>
</body>
</html>
