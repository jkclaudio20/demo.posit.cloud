<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit PDC</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'edit-pdc-form');
        echo admin_form_open_multipart("purchases/ajax_edit_pdc/" . $pdc->id, $attrib); ?>
        <div class="modal-body">
            <?php if ($pdc !== FALSE): ?>
                <div class="form-group">
                    <label for="cheque_no">Cheque No.</label>
                    <?php echo form_input('cheque_no', $pdc->cheque_no, 'class="form-control" id="cheque_no" placeholder="Cheque No."'); ?>
                </div>
                <div class="form-group">
                    <label for="clearing_date">Clearing Date</label>
                    <?php echo form_input('clearing_date', $this->sma->hrsd($pdc->date), 'class="form-control date-noearly" id="clearing_date" placeholder="Clearing Date" required'); ?>
                </div>
                <?php if ($inv->payment_status == 'paid'): ?>
                    <span class="label label-danger">This PDC is already marked as paid. If you edit this PDC, previous payment will be discarded.</span>
                <?php endif; ?>
            <?php else: ?>
                <i class="fa fa-warning"></i> This purchase doesn't have any PDC.
            <?php endif; ?>
        </div>
        <?php if ($pdc !== FALSE): ?>
            <div class="modal-footer">
                <?php echo form_submit('add_payment', lang('submit'), 'class="btn btn-primary"'); ?>
            </div>
        <?php endif; ?>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
