<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit PDC</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'delete-pdc-form');
        echo admin_form_open_multipart("purchases/ajax_delete_pdc/" . $inv->id, $attrib); ?>
        <div class="modal-body">
            <?php if ($inv !== FALSE): ?>
                <?php if ($inv->pdc == 1): ?>
                    <?php echo form_hidden('purchase_id', $inv->id); ?>
                    You are about to delete PDC for this purchase. This action is irreversible. Do you want to proceed?
                <?php else: ?>
                    This purchase doesn't have any PDC
                <?php endif; ?>
            <?php else: ?>
                <i class="fa fa-warning"></i> This purchase does not exist.
            <?php endif; ?>
        </div>
        <?php if ($inv !== FALSE): ?>
            <?php if ($inv->pdc == 1): ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <?php echo form_submit('delete_pdc', 'Delete', 'class="btn btn-danger"'); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
