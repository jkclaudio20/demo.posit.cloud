<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#POData').dataTable({
            "aaSorting": [[6, "desc"], [1, "desc"], [2, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=admin_url('purchases/getPdcs' . ($warehouse_id ? '/' . $warehouse_id : ''))?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false,"mRender": checkbox}, {"mRender": fld}, null, null, {"mRender": row_status}, {"mRender": currencyFormat}, {"mRender": fsd}, {"mRender": pdc_status}, {"bSortable": false,"mRender": attachment}, {"bSortable": false}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "purchase_link";
                return nRow;
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var paid = 0;
                for (var i = 0; i < aaData.length; i++) {
                    paid += parseFloat(aaData[aiDisplay[i]][5]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(paid);
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('ref_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('purchase_status');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 7, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer");

        <?php if ($this->session->userdata('remove_pols')) {?>
        if (localStorage.getItem('poitems')) {
            localStorage.removeItem('poitems');
        }
        if (localStorage.getItem('podiscount')) {
            localStorage.removeItem('podiscount');
        }
        if (localStorage.getItem('potax2')) {
            localStorage.removeItem('potax2');
        }
        if (localStorage.getItem('poshipping')) {
            localStorage.removeItem('poshipping');
        }
        if (localStorage.getItem('poref')) {
            localStorage.removeItem('poref');
        }
        if (localStorage.getItem('powarehouse')) {
            localStorage.removeItem('powarehouse');
        }
        if (localStorage.getItem('ponote')) {
            localStorage.removeItem('ponote');
        }
        if (localStorage.getItem('posupplier')) {
            localStorage.removeItem('posupplier');
        }
        if (localStorage.getItem('pocurrency')) {
            localStorage.removeItem('pocurrency');
        }
        if (localStorage.getItem('poextras')) {
            localStorage.removeItem('poextras');
        }
        if (localStorage.getItem('podate')) {
            localStorage.removeItem('podate');
        }
        if (localStorage.getItem('postatus')) {
            localStorage.removeItem('postatus');
        }
        if (localStorage.getItem('popayment_term')) {
            localStorage.removeItem('popayment_term');
        }
        <?php $this->sma->unset_data('remove_pols');}
        ?>
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
	    echo admin_form_open('purchases/purchase_actions', 'id="action-form"');
	}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-star"></i><?=lang('purchases') . ' with PDCs' . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')';?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=admin_url('purchases/add')?>">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_purchase')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<?=lang("delete_purchases")?>"
                                data-content="<p><?=lang('r_u_sure')?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?=lang('delete_purchases')?>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if (!empty($warehouses)) {
                    ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" data-container="body" title="<?= lang("warehouses") ?><?php echo (INSTALLATION == 'demo') ? ' (Only available for POSit Cloud Editions)' : ''; ?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?=admin_url('purchases')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                            <li class="divider"></li>
                            <?php
                            	foreach ($warehouses as $warehouse) {
                            	        echo '<li ' . ($warehouse_id && $warehouse_id == $warehouse->id ? 'class="active"' : '') . '><a href="' . admin_url('purchases/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            	    }
                                ?>
                        </ul>
                    </li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?=lang('list_results');?></p>

                <div class="table-responsive">
                    <table id="POData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th><?= lang("date"); ?></th>
                            <th><?= lang("ref_no"); ?></th>
                            <th><?= lang("supplier"); ?></th>
                            <th><?= lang("purchase_status"); ?></th>
                            <th><?= lang("paid"); ?></th>
                            <th>Clearing Date</th>
                            <th><?= lang("payment_status"); ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                            <th style="width:100px;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="11" class="dataTables_empty"><?=lang('loading_data_from_server');?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?= lang("paid"); ?></th>
                            <th></th>
                            <th></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<?php if ($pending_pdcs): ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        PNotify.removeAll();

        new PNotify('Some PDCs are not yet cleared. Clearing them now...');

        jQuery.ajax({
            url      : '<?php echo admin_url('purchases/clear_pending_pdcs'); ?>',
            dataType : 'JSON',
            success  : function(data)
            {
                PNotify.removeAll();
                if (data.result == 'success')
                {
                    new PNotify({
                        title: 'Success',
                        text : data.message,
                        type : 'success'
                    });

                    oTable.fnReloadAjax();
                }
                else
                {
                    new PNotify({
                        title: 'Error',
                        text : data.message,
                        type : 'error'
                    });
                }
            },
            error    : function()
            {
                PNotify.removeAll();
                new PNotify({
                    title : 'Error',
                    text  : 'Unable to clear them now. Will try again later.',
                    type  : 'error'
                });
            }
        });
    });
</script>
<?php endif; ?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(document).on('submit', '#edit-pdc-form', function(e){
            e.preventDefault();

            var form      = jQuery(this);
            var form_data = new FormData(this);
            var modal     = form.parents('.modal');

            form.find('[type=submit]').val('Please wait...').prop('disabled', true);

            jQuery.ajax({
                url      : form.attr('action'),
                type     : 'POST',
                dataType : 'JSON',
                data     : form_data,
                cache    : false,
                processData: false,
                contentType: false,
                success  : function(data)
                {
                    form.find('[type=submit]').val('Submit').prop('disabled', false);
                    if (data.result == 'success')
                    {
                        modal.modal('hide');
                        oTable.fnReloadAjax();
                        PNotify.removeAll();
                        new PNotify({
                            title : 'Success',
                            text  : data.message,
                            type  : 'success'
                        });
                    }
                    else
                    {
                        PNotify.removeAll();
                        new PNotify({
                            title : 'Error',
                            text  : data.message,
                            type  : 'error'
                        });
                    }
                },
                error    : function()
                {
                    form.find('[type=submit]').val('Submit').prop('disabled', false);
                    PNotify.removeAll();
                    new PNotify({
                        title : 'Error',
                        text  : 'Something went wrong. Please try again.',
                        type  : 'error'
                    });
                }
            })
        });

        jQuery(document).on('submit', '#delete-pdc-form', function(e){
            e.preventDefault();

            var form      = jQuery(this);
            var form_data = new FormData(this);
            var modal     = form.parents('.modal');

            form.find('[type=submit]').val('Please wait...').prop('disabled', true);

            jQuery.ajax({
                url      : form.attr('action'),
                type     : 'POST',
                dataType : 'JSON',
                data     : form_data,
                cache    : false,
                processData: false,
                contentType: false,
                success  : function(data)
                {
                    form.find('[type=submit]').val('Delete').prop('disabled', false);
                    if (data.result == 'success')
                    {
                        modal.modal('hide');
                        oTable.fnReloadAjax();
                        PNotify.removeAll();
                        new PNotify({
                            title : 'Success',
                            text  : data.message,
                            type  : 'success'
                        });
                    }
                    else
                    {
                        PNotify.removeAll();
                        new PNotify({
                            title : 'Error',
                            text  : data.message,
                            type  : 'error'
                        });
                    }
                },
                error    : function()
                {
                    form.find('[type=submit]').val('Submit').prop('disabled', false);
                    PNotify.removeAll();
                    new PNotify({
                        title : 'Error',
                        text  : 'Something went wrong. Please try again.',
                        type  : 'error'
                    });
                }
            })
        });
    });
</script>
