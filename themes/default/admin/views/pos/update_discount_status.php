<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Update Discount Status</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("pos/update_discount_status/" . $inv->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= lang('sale_details'); ?>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-borderless" style="margin-bottom:0;">
                        <tbody>
                            <tr>
                                <td><?= lang('reference_no'); ?></td>
                                <td><?= $inv->reference_no; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('biller'); ?></td>
                                <td><?= $inv->biller; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('customer'); ?></td>
                                <td><?= $inv->customer; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('grand_total'); ?></td>
                                <td><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('total_discount'); ?></td>
                                <td><strong><?= $this->sma->formatMoney($inv->total_discount); ?></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if ($returned) { ?>
                <h4><?= lang('sale_x_action'); ?></h4>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="discount_status">Action</label>
                            <?php
                            $opts = array('1' => 'Approve Discount', '3' => 'Reject Discount');
                            if (INSTALLATION == 'local') $opts['4'] = 'Request Approval from Cloud';
                            ?>
                            <?= form_dropdown('discount_status', $opts, $inv->discount_status, 'class="form-control" id="discount_status" required="required" style="width:100%;"'); ?>
                        </div>
                    </div>
                    <?php if ( ! $Owner && ! ($this->GP['sales-approve_discount'])): ?>
                        <div class="discount_credentials">
                            <div class="col-md-6">
                                <label for="user_id">Username</label>
                                <div class="form-group">
                                    <?= form_dropdown('user_id', $approvers, '', 'class="form-control" id="user_id" required="required" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="password">Password</label>
                                <div class="form-group">
                                    <?= form_input(['name' => 'password', 'id' => 'password', 'type' => 'password'], '', 'class="form-control" autocomplete="new-password" required'); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php } ?>

        </div>
        <?php if ( ! $returned) { ?>
        <div class="modal-footer">
            <?php echo form_submit('update', lang('update'), 'class="btn btn-primary"'); ?>
        </div>
        <?php } ?>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var container = $('.discount_credentials');
        var discount_status = $('#discount_status').val();

        if (discount_status == 4)
        {
            container.find('input').prop({'required': false, 'disabled': true});
            container.find('select').prop({'required': false, 'disabled': true});
            container.slideUp();
        }
        else
        {
            container.find('input').prop({'required': true, 'disabled': false});
            container.find('select').prop({'required': true, 'disabled': false});
            container.slideDown();
        }

        $('#discount_status').on('change', function(){
            if ($(this).val() == 4)
            {
                container.find('input').prop({'required': false, 'disabled': true});
                container.find('select').prop({'required': false, 'disabled': true});
                container.slideUp();
            }
            else
            {
                container.find('input').prop({'required': true, 'disabled': false});
                container.find('select').prop({'required': true, 'disabled': false});
                container.slideDown();
            }
        });
    });
</script>
<?= $modal_js ?>
