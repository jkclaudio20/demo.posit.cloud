<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>$(document).ready(function () {
        CURI = '<?= admin_url('reports/profit_loss'); ?>';
    });</script>
<style>@media print {
        .fa {
            color: #EEE;
            display: none;
        }

        .small-box {
            border: 1px solid #CCC;
        }
    }</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bars"></i><?= lang('profit_loss'); ?></h2>

        <div class="box-icon">
            <div class="form-group choose-date hidden-xs">
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text"
                               value="<?= ($start ? $this->sma->hrld($start) : '') . ' - ' . ($end ? $this->sma->hrld($end) : ''); ?>"
                               id="daterange" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                        <i class="icon fa fa-file-picture-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('view_pl_report'); ?></p>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('purchases') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_purchases->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Purchases') ?></th>
                                        <th style="text-align: right;"><?= $total_purchases->total ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Paid Amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_purchases->paid) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_purchases->tax) ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('payments_sent') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_paid->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Sent') ?></th>
                                        <th style="text-align: right;"><?= $total_paid->total ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('expenses') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_expenses->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Expenses') ?></th>
                                        <th style="text-align: right;"><?= $total_expenses->total ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('sales') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Sales') ?></th>
                                        <th style="text-align: right;"><?= $total_sales->total ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Paid Amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->paid) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('payments_received') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Received') ?></th>
                                        <th style="text-align: right;"><?= $total_received->total ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('cash') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received_cash->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('CC') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received_cc->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('cheque') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received_cheque->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('paypal_pro') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received_ppp->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('stripe') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_received_stripe->total_amount) ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('returns') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('total_amount') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_return_sales->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('Total Returns') ?></th>
                                        <th style="text-align: right;"><?= $total_return_sales->total ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_return_sales->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('profit_loss') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('sales') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('purchases') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_purchases->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('total') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount - $total_purchases->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('profit_loss_s') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('sales') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_sales->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('purchases') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_purchases->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('total') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount - $total_purchases->total_amount - $total_sales->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                        <th style="background-color: #f3f3f3;">&nbsp;</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('profit_loss_sp') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?= lang('sales') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney($total_sales->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_sales->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('purchases') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_purchases->total_amount) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('tax') ?></th>
                                        <th style="text-align: right;">- <?= $this->sma->formatMoney($total_purchases->tax) ?></th>
                                    </tr>
                                    <tr>
                                        <th><?= lang('total') ?></th>
                                        <th style="text-align: right;"><?= $this->sma->formatMoney(($total_sales->total_amount - $total_sales->tax) - ($total_purchases->total_amount - $total_purchases->tax)) ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('cashflow') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th style="text-align: center;"><?= $this->sma->formatMoney($total_received->total_amount) . ' ' . lang('sales') ?>
                                                        - <?= $this->sma->formatMoney(0-$total_returned->total_amount) . ' ' . lang('refund') ?>
                                                        - <?= $this->sma->formatMoney($total_paid->total_amount) . ' ' . lang('purchases') ?>
                                                        - <?= $this->sma->formatMoney($total_expenses->total_amount) . ' ' . lang('expenses') ?>
                                                        - <?= $this->sma->formatMoney($total_return_sales->total_amount) . ' ' . lang('returns') ?>
                                                        = <?= $this->sma->formatMoney($total_received->total_amount - (0-$total_returned->total_amount) - $total_paid->total_amount - $total_expenses->total_amount - ($total_return_sales ? $total_return_sales->total_amount: 0)) ?>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #0eace6; background-color: #0eace6;"><?= lang('profit_report') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th style="text-align: center;"><?= $this->sma->formatMoney($total_received->total_amount) . ' ' . lang('sales') ?>
                                                        - <?= $this->sma->formatMoney(0-$total_returned->total_amount) . ' ' . lang('refund') ?>
                                                        - <?= $this->sma->formatMoney($total_cost->total_amount) . ' ' . lang('cost') ?>
                                                        - <?= $this->sma->formatMoney($total_expenses->total_amount) . ' ' . lang('expenses') ?>
                                                        - <?= $this->sma->formatMoney($total_return_sales->total_amount) . ' ' . lang('returns') ?>
                                                        = <?= $this->sma->formatMoney($total_received->total_amount - (0-$total_returned->total_amount) - $total_cost->total_amount - $total_expenses->total_amount - ($total_return_sales ? $total_return_sales->total_amount: 0)) ?>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="border-color: #428BCA; background-color: #428BCA;"><?= lang('profit_loss') . " per ". lang('warehouse') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <?php foreach ($warehouses_report as $warehouse_report) { ?>
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2" style="border-color: #428BCA; background-color: #428BCA;"><?= $warehouse_report['warehouse']->name.' ('.$warehouse_report['warehouse']->code.')'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="border-style: none none solid none; border-color: black;">
                                    <th><?= lang('sales').' - '.lang('purchases').' - '.lang('returns'); ?></th>
                                    <th style="min-width: 100px; text-align: right;"><?= $this->sma->formatMoney(($warehouse_report['total_sales']->total_amount) - ($warehouse_report['total_purchases']->total_amount) - ($warehouse_report['total_returns']->total_amount)) ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #ddddf1;"><?= lang('sales'); ?></th>
                                    <th style="text-align: right; background-color: #ddddf1;"><?= $this->sma->formatMoney($warehouse_report['total_sales']->total_amount); ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #ddddf1;"><?= lang('tax'); ?></th>
                                    <th style="text-align: right; background-color: #ddddf1;">- <?= $this->sma->formatMoney($warehouse_report['total_sales']->tax) ?></th>
                                </tr>
                                <tr style="border-style: double none solid none; border-color: black;">
                                    <th style="background-color: #ddddf1;"><?= lang('net_sales'); ?></th>
                                    <th style="text-align: right; background-color: #ddddf1;"><?= $this->sma->formatMoney($warehouse_report['total_sales']->total_amount-$warehouse_report['total_sales']->tax); ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #e6eafb;"><?= lang('purchases') ?></th>
                                    <th style="text-align: right; background-color: #e6eafb;"><?= $this->sma->formatMoney($warehouse_report['total_purchases']->total_amount) ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #e6eafb;"><?= lang('tax'); ?></th>
                                    <th style="text-align: right; background-color: #e6eafb;">- <?= $this->sma->formatMoney($warehouse_report['total_purchases']->tax) ?></th>
                                </tr>
                                <tr style="border-style: double none solid none; border-color: black;">
                                    <th style="background-color: #e6eafb;"><?= lang('net_purchases'); ?></th>
                                    <th style="text-align: right; background-color: #e6eafb;"><?= $this->sma->formatMoney($warehouse_report['total_purchases']->total_amount-$warehouse_report['total_purchases']->tax); ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #efefef;"><?= lang('returns') ?></th>
                                    <th style="text-align: right; background-color: #efefef;"><?= $this->sma->formatMoney($warehouse_report['total_returns']->total_amount) ?></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #efefef;"><?= lang('tax'); ?></th>
                                    <th style="text-align: right; background-color: #efefef;">- <?= $this->sma->formatMoney($warehouse_report['total_returns']->tax) ?></th>
                                </tr>
                                <tr style="border-style: double none solid none; border-color: black;">
                                    <th style="background-color: #efefef;"><?= lang('net_returns'); ?></th>
                                    <th style="text-align: right; background-color: #efefef;"><?= $this->sma->formatMoney($warehouse_report['total_returns']->total_amount-$warehouse_report['total_returns']->tax); ?></th>
                                </tr>
                                <tr style="border-style: solid none solid none; border-color: black;">
                                    <th><?= lang('net_sales').' - '.lang('net_purchases').' - '.lang('net_returns'); ?></th>
                                    <th style="text-align: right;"><?= $this->sma->formatMoney((($warehouse_report['total_sales']->total_amount-$warehouse_report['total_sales']->tax))-($warehouse_report['total_purchases']->total_amount-$warehouse_report['total_purchases']->tax)-($warehouse_report['total_returns']->total_amount-$warehouse_report['total_returns']->tax)); ?></th>
                                </tr>
                                <tr>
                                    <th><?= lang('Total Expenses'); ?></th>
                                    <th style="text-align: right;"><?= $warehouse_report['total_expenses']->total; ?></th>
                                </tr>
                                <tr>
                                    <th><?= lang('expenses'). " " .lang('amount'); ?></th>
                                    <th style="text-align: right;"><?= $this->sma->formatMoney($warehouse_report['total_expenses']->total_amount); ?></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/profit_loss_pdf')?>/" + encodeURIComponent('<?=$start?>') + "/" + encodeURIComponent('<?=$end?>');
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>
