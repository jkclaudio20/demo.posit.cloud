<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends MY_Controller
{
	public $user_id;
	public $member_group_id;

	function __construct()
	{
		parent::__construct();

		if ( ! $this->loggedIn)
		{
			$this->user_id = NULL;

			$this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
		}
		else
		{
			$this->user_id = $this->ion_auth->get_user_id();
		}

		$this->config->load('pos', TRUE);
		$this->member_group_id = $this->config->item('member_group_id', 'pos');
		$this->load->admin_model('membership_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
	}

	// Members List
	function index()
	{
		$meta['page_title'] = 'Members List';

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

		$this->page_construct('membership/index', $meta, $this->data);
	}

	// Add New Member
	function add_member()
	{
		$meta['page_title'] = 'Add New Member';

		// Form Fields

		// Personal Information
		$this->data['first_name'] = array(
			'name' => 'first_name',
			'id'   => 'first_name',
			'type' => 'text'
		);

		$this->data['middle_name'] = array(
			'name' => 'middle_name',
			'id'   => 'middle_name',
			'type' => 'text'
		);

		$this->data['last_name'] = array(
			'name' => 'last_name',
			'id'   => 'last_name',
			'type' => 'text'
		);

		$this->data['birthdate'] = array(
			'name' => 'birthdate',
			'id'   => 'birthdate',
			'type' => 'text'
		);

		// Contact Information
		$this->data['street'] = array(
			'name' => 'street',
			'id'   => 'street',
			'type' => 'text'
		);

		$this->data['barangay'] = array(
			'name' => 'barangay',
			'id'   => 'barangay',
			'type' => 'text'
		);

		$this->data['city'] = array(
			'name' => 'city',
			'id'   => 'city',
			'type' => 'text'
		);

		$this->data['province'] = array(
			'name' => 'province',
			'id'   => 'province',
			'type' => 'text'
		);

		$this->data['zip'] = array(
			'name' => 'zip',
			'id'   => 'zip',
			'type' => 'text'
		);

		$this->data['phone'] = array(
			'name' => 'phone',
			'id'   => 'phone',
			'type' => 'text'
		);

		$this->data['email'] = array(
			'name' => 'email',
			'id'   => 'email',
			'type' => 'text'
		);

		// Physical Information
		$this->data['weight'] = array(
			'name' => 'weight',
			'id'   => 'weight',
			'type' => 'number'
		);

		$this->data['height'] = array(
			'name' => 'height',
			'id'   => 'height',
			'type' => 'number'
		);

		$this->data['chest'] = array(
			'name' => 'chest',
			'id'   => 'chest',
			'type' => 'number'
		);

		$this->data['waist'] = array(
			'name' => 'waist',
			'id'   => 'waist',
			'type' => 'number'
		);

		$this->data['thigh'] = array(
			'name' => 'thigh',
			'id'   => 'thigh',
			'type' => 'number'
		);

		$this->data['arms'] = array(
			'name' => 'arms',
			'id'   => 'arms',
			'type' => 'number'
		);

		$this->data['fat'] = array(
			'name' => 'fat',
			'id'   => 'fat',
			'type' => 'number'
		);

		// Membership Information
		$this->data['join_date'] = array(
			'name' => 'join_date',
			'id'   => 'join_date',
			'type' => 'text'
		);

		$this->data['membership_types'] = $this->membership_model->get_membership_types(TRUE, TRUE);

		$this->data['from'] = array(
			'name' => 'from',
			'id'   => 'from',
			'type' => 'text'
		);

		$this->data['to'] = array(
			'name' => 'to',
			'id'   => 'to',
			'type' => 'text'
		);

		// Login Information
		$this->data['username'] = array(
			'name' => 'username',
			'id'   => 'username',
			'type' => 'text'
		);

		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);

		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->page_construct('membership/add_member', $meta, $this->data);
	}

	// Feed Membership Types Table with JSON Data
	function json_data()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$output['aaData'] = array();

			// Fetch Data from DB
			$membership_types = $this->membership_model->get_membership_types();

			if ($membership_types !== FALSE)
			{
				$counter = 1;

				foreach ($membership_types as $row)
				{
					$output['aaData'][] = array(
						'counter'     => $counter++.'.',
						'name'        => htmlspecialchars($row->membership_name),
						'description' => ($row->description) ? htmlspecialchars($row->description) : '<span class="text-muted">No description available</span>',
						'actions'     => '
							<button type="button" class="btn blue btn-xs btn-edit-membership" data-id="'.$row->membership_type_id.'" data-toggle="modal" data-target="#edit-membership-modal"><i class="fa fa-pencil"></i></button>
							<button type="button" class="btn info btn-xs btn-view-membership" data-id="'.$row->membership_type_id.'" data-toggle="modal" data-target="#view-membership-modal"><i class="fa fa-eye"></i></button>
							<button type="button" class="btn danger btn-xs btn-delete-membership" data-id="'.$row->membership_type_id.'" data-name="'.$row->membership_name.'" data-toggle="modal" data-target="#delete-membership-modal"><i class="fa fa-trash-o"></i></button>
						'
					);
				}
			}

			echo json_encode($output);
			exit();
		}
	}

	// Show New Membership Type Form
	function add()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$this->data['membership_name'] = array(
				'name' => 'membership_name',
				'id'   => 'membership_name',
				'type' => 'text'
			);

			$this->data['period'] = array(
				'name' => 'period',
				'id'   => 'period',
				'type' => 'number'
			);

			$this->data['no_of_classes'] = array(
				'name' => 'no_of_classes',
				'id'   => 'no_of_classes',
				'type' => 'number'
			);

			$this->data['limit_types'] = array(
				'weekly'  => 'every week',
				'monthly' => 'every month'
			);

			$this->data['amount'] = array(
				'name' => 'amount',
				'id'   => 'amount',
				'type' => 'text'
			);

			$this->data['signup_fee'] = array(
				'name' => 'signup_fee',
				'id'   => 'signup_fee',
				'type' => 'text'
			);

			$this->data['description'] = array(
				'name' => 'description',
				'id'   => 'description',
				'rows' => 5
			);

			$output['html'] = $this->load->view($this->theme.'membership/add', $this->data, TRUE);

			echo json_encode($output);
			exit();
		}
	}

	// Add New Membership Type
	function ajax_add()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			// Start Form Validation
			$this->form_validation->set_rules('membership_name', 'Membership Name', 'trim|required');
			$this->form_validation->set_rules('period', 'Membership Period', 'trim|is_natural|required');
			$this->form_validation->set_rules('limited', 'Limit', 'required');

			if ($this->input->post('limited') == 1)
			{
				$this->form_validation->set_rules('no_of_classes', 'No. of Classes', 'trim|is_natural|required');
				$this->form_validation->set_rules('limit_type', 'Limit Type', 'required');
			}

			$this->form_validation->set_rules('amount', 'Membership Amount', 'trim|numeric|required');
			$this->form_validation->set_rules('signup_fee', 'Signup Fee', 'trim|numeric|required');
			$this->form_validation->set_rules('description', 'Description', 'trim');

			if ($this->form_validation->run() === TRUE)
			{
				$now = new DateTime();

				$insert_data = array(
					'membership_name' => $this->input->post('membership_name'),
					'period'          => $this->input->post('period'),
					'limited'         => $this->input->post('limited'),
					'no_of_classes'   => ($this->input->post('limited') == 1) ? $this->input->post('no_of_classes') : NULL,
					'limit_type'      => ($this->input->post('limited') == 1) ? $this->input->post('limit_type') : NULL,
					'amount'          => $this->input->post('amount'),
					'signup_fee'      => $this->input->post('signup_fee'),
					'description'     => ($this->input->post('description')) ? $this->input->post('description') : NULL,
					'date_created'    => $now->format('Y-m-d H:i:s'),
					'created_by'      => $this->user_id,
					'status'          => 'active'
				);

				if ($this->membership_model->add_membership_type($insert_data) === TRUE)
				{
					$output['result'] = 'success';
					$output['message'] = $this->ion_auth->messages();

					echo json_encode($output);
					exit();
				}
				else
				{
					$output['result'] = 'error';
					$output['message'] = $this->ion_auth->errors();

					echo json_encode($output);
					exit();
				}
			}
			else
			{
				$output['result']  = 'error';
				$output['message'] = validation_errors();

				echo json_encode($output);
				exit();
			}
		}
	}

	// View Membership Type Details
	function view()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$membership_type_id = $this->input->post('id');

			$this->data['data'] = $this->membership_model->get_membership_type_data($membership_type_id);

			$output['html'] = $this->load->view($this->theme.'membership/view', $this->data, TRUE);

			echo json_encode($output);
			exit();
		}
	}

	// Show Edit Membership Type Form
	function edit()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$membership_type_id = $this->input->post('id');

			$this->data['data'] = $this->membership_model->get_membership_type_data($membership_type_id);

			if ($this->data['data'] !== FALSE)
			{
				$this->data['membership_name'] = array(
					'name' => 'membership_name',
					'id'   => 'membership_name',
					'type' => 'text'
				);

				$this->data['period'] = array(
					'name' => 'period',
					'id'   => 'period',
					'type' => 'number'
				);

				$this->data['no_of_classes'] = array(
					'name' => 'no_of_classes',
					'id'   => 'no_of_classes',
					'type' => 'number'
				);

				$this->data['limit_types'] = array(
					'weekly'  => 'every week',
					'monthly' => 'every month'
				);

				$this->data['amount'] = array(
					'name' => 'amount',
					'id'   => 'amount',
					'type' => 'text'
				);

				$this->data['signup_fee'] = array(
					'name' => 'signup_fee',
					'id'   => 'signup_fee',
					'type' => 'text'
				);

				$this->data['description'] = array(
					'name' => 'description',
					'id'   => 'description',
					'rows' => 5
				);
			}


			$output['result'] = ($this->data['data'] !== FALSE) ? 'success' : 'error';
			$output['html']   = $this->load->view($this->theme.'membership/edit', $this->data, TRUE);

			echo json_encode($output);
			exit();
		}
	}

	// Update Membership Type
	function ajax_edit()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			// Start Form Validation
			$this->form_validation->set_rules('membership_name', 'Membership Name', 'trim|required');
			$this->form_validation->set_rules('period', 'Membership Period', 'trim|is_natural|required');
			$this->form_validation->set_rules('limited', 'Limit', 'required');

			if ($this->input->post('limited') == 1)
			{
				$this->form_validation->set_rules('no_of_classes', 'No. of Classes', 'trim|is_natural|required');
				$this->form_validation->set_rules('limit_type', 'Limit Type', 'required');
			}

			$this->form_validation->set_rules('amount', 'Membership Amount', 'trim|numeric|required');
			$this->form_validation->set_rules('signup_fee', 'Signup Fee', 'trim|numeric|required');
			$this->form_validation->set_rules('description', 'Description', 'trim');

			if ($this->form_validation->run() === TRUE)
			{
				$membership_type_id = $this->input->post('id');

				$update_data = array(
					'membership_name' => $this->input->post('membership_name'),
					'period'          => $this->input->post('period'),
					'limited'         => $this->input->post('limited'),
					'no_of_classes'   => ($this->input->post('limited') == 1) ? $this->input->post('no_of_classes') : NULL,
					'limit_type'      => ($this->input->post('limited') == 1) ? $this->input->post('limit_type') : NULL,
					'amount'          => $this->input->post('amount'),
					'signup_fee'      => $this->input->post('signup_fee'),
					'description'     => ($this->input->post('description')) ? $this->input->post('description') : NULL,
					'status'          => 'active'
				);

				if ($this->membership_model->update_membership_type($membership_type_id, $update_data) === TRUE)
				{
					$output['result'] = 'success';
					$output['message'] = $this->ion_auth->messages();

					echo json_encode($output);
					exit();
				}
				else
				{
					$output['result'] = 'error';
					$output['message'] = $this->ion_auth->errors();

					echo json_encode($output);
					exit();
				}
			}
			else
			{
				$output['result']  = 'error';
				$output['message'] = validation_errors();

				echo json_encode($output);
				exit();
			}
		}
	}

	// Membership Type deletion confirmed
	function ajax_delete()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$membership_type_id = $this->input->post('id');
			$membership_name    = $this->input->post('name');

			if ($this->membership_model->delete_membership_type($membership_type_id, $membership_name))
			{
				$output['result']  = 'success';
				$output['message'] = $this->ion_auth->messages();

				echo json_encode($output);
				exit();
			}
			else
			{
				$output['result']  = 'success';
				$output['message'] = $this->ion_auth->messages();

				echo json_encode($output);
				exit();
			}
		}
	}

	// Add New Member
	function ajax_add_member()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middle_name', 'Middle Name', 'trim');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
			$this->form_validation->set_rules('birthdate', 'Date of Birth', 'trim|required');
			$this->form_validation->set_rules('street', 'Street', 'trim');
			$this->form_validation->set_rules('barangay', 'Barangay', 'trim');
			$this->form_validation->set_rules('city', 'City / Municipality', 'trim|required');
			$this->form_validation->set_rules('province', 'Province', 'trim|required');
			$this->form_validation->set_rules('zip', 'ZIP Code', 'trim');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('weight', 'Weight', 'trim|numeric');
			$this->form_validation->set_rules('height', 'Height', 'trim|numeric');
			$this->form_validation->set_rules('chest', 'Chest', 'trim|numeric');
			$this->form_validation->set_rules('waist', 'Waist', 'trim|numeric');
			$this->form_validation->set_rules('thigh', 'Thigh', 'trim|numeric');
			$this->form_validation->set_rules('arms', 'Arms', 'trim|numeric');
			$this->form_validation->set_rules('fat', 'Fat', 'trim|numeric');
			$this->form_validation->set_rules('join_date', 'Join Date', 'trim|required');
			$this->form_validation->set_rules('membership_type_id', 'Membership Type', 'required');
			$this->form_validation->set_rules('username', 'Username', 'trim|alpha_numeric|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|matches[password]|required');

			if ($this->form_validation->run() === TRUE)
			{
				// Register Member as User first
				$username = strtolower($this->input->post('username'));
	            $email    = strtolower($this->input->post('email'));
	            $password = $this->input->post('password');

	            $additional_data = array(
	                'first_name' => $this->input->post('first_name'),
	                'last_name'  => $this->input->post('last_name'),
	                'company'    => $this->input->post('company'),
	                'phone'      => $this->input->post('phone'),
	                'gender'     => $this->input->post('gender'),
	                'group_id'   => $this->member_group_id
	            );

				$user_id = $this->ion_auth->register($username, $password, $email, $additional_data);

				if ($user_id !== FALSE)
				{
					// Prepare Data
					$birthdate = new DateTime($this->input->post('birthdate'));
					$join_date = new DateTime($this->input->post('join_date'));
					$now       = new DateTime();

					// Member Data
					$member_data = array(
						'member_user_id' => $user_id,
						'first_name'     => $this->input->post('first_name'),
						'middle_name'    => $this->input->post('middle_name'),
						'last_name'      => $this->input->post('last_name'),
						'gender'         => $this->input->post('gender'),
						'birthdate'      => $birthdate->format('Y-m-d'),
						'street'         => $this->input->post('street'),
						'barangay'       => $this->input->post('barangay'),
						'city'           => $this->input->post('city'),
						'province'       => $this->input->post('province'),
						'zip'            => $this->input->post('zip'),
						'phone'          => $this->input->post('phone'),
						'email'          => $this->input->post('email'),
						'weight'         => $this->input->post('weight'),
						'height'         => $this->input->post('height'),
						'chest'          => $this->input->post('chest'),
						'waist'          => $this->input->post('waist'),
						'thigh'          => $this->input->post('thigh'),
						'arms'           => $this->input->post('arms'),
						'fat'            => $this->input->post('fat'),
						'join_date'      => $join_date->format('Y-m-d'),
						'date_created   '=> $now->format('Y-m-d H:i:s'),
						'created_by'     => $this->user_id
					);

					// Membership History Data
					$from = new DateTime($this->input->post('from'));
					$to   = new DateTime($this->input->post('to'));

					$membership_data = array(
						'membership_type_id' => $this->input->post('membership_type_id'),
						'from'               => $from->format('Y-m-d'),
						'to'                 => $to->format('Y-m-d'),
						'created_by'         => $this->user_id
					);

					if ($this->membership_model->add_member($member_data, $membership_data))
					{
						$this->ion_auth->clear_messages();
						$this->session->set_flashdata('result', 'success');
						$this->session->set_flashdata('message', $this->ion_auth->messages());

						$output['result'] = 'success';
						$output['url']    = admin_url('members');

						echo json_encode($output);
						exit();
					}
					else
					{
						$output['result']  = 'error';
						$output['message'] = $this->ion_auth->errors();

						echo json_encode($output);
						exit();
					}
				}
			}
			else
			{
				$output['result']  = 'error';
				$output['message'] = validation_errors();

				echo json_encode($output);
				exit();
			}
		}
	}

	// Feed Members Table with JSON Data
	function members_json_data()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$output['aaData'] = array();

			$members = $this->membership_model->get_members();

			if ($members !== FALSE)
			{
				$counter = 1;

				foreach ($members as $row)
				{
					$middle_name = ( ! empty($row->middle_name)) ? ' '.$row->middle_name : NULL;
					$join_date   = new DateTime($row->join_date);

					$output['aaData'][] = array(
						'counter'   => $counter++.'.',
						'name'      => $row->first_name.$middle_name.' '.$row->last_name,
						'join_date' => $join_date->format('F j, Y'),
						'status'    => ($row->active == 1) ? '<span class="label primary">Active</span>' : '<span class="label danger">Inactive</span>',
						'actions'   => '
							<div class="dropdown inline">
								<button type="button" class="btn btn-sm success dropdown-toggle" data-toggle="dropdown">Actions </button>
								<div class="dropdown-menu" role="menu">
									<a href="'.admin_url('members/view?id=').$row->member_id.'" class="dropdown-item">
										<i class="fa fa-eye"></i> View Information
									</a>
									<a href="'.admin_url('members/edit?id=').$row->member_id.'" class="dropdown-item">
										<i class="fa fa-pencil"></i> Edit Information
									</a>
									<a href="#" data-toggle="modal" data-target="#update-membership-modal" data-id="'.$row->member_id.'" class="dropdown-item">
										<i class="fa fa-refresh"></i> Update Membership
									</a>
									<div class="dropdown-divider"></div>
									<a href="#" data-toggle="modal" data-target="#delete-member-modal" data-id="'.$row->member_id.'" data-name="'.$row->first_name.' '.$row->last_name.'" class="dropdown-item">
										<i class="fa fa-trash-o"></i> Delete Member
									</a>
								</div>
							</div>
						'
					);
				}
			}

			echo json_encode($output);
			exit();
		}
	}

	// View Member Information
	function view_member()
	{
		$meta['page_title'] = 'Member Information';

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

		$this->data['member_id'] = $this->input->get('id');
		$this->data['data']      = $this->membership_model->get_member_data($this->data['member_id']);

		$this->page_construct('membership/view_member', $meta, $this->data);
	}

	// Feed Membership History Table with JSON Data
	function membership_history_json_data()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$member_id = $this->input->get('id');

			$history = $this->membership_model->get_membership_history($member_id);

			$output['aaData'] = array();

			if ($history !== FALSE)
			{
				$counter = 1;

				foreach ($history as $row)
				{
					$from = new DateTime($row->from);
					$to   = new DateTime($row->to);

					$output['aaData'][] = array(
						'counter'         => $counter++.'.',
						'validity'        => ($from->format('Y') == $to->format('Y')) ? $from->format('F j').' to '.$to->format('F j, Y') : $from->format('F j, Y').' to '.$to->format('F j, Y'),
						'membership_type' => htmlspecialchars($row->membership_name)
					);
				}
			}

			echo json_encode($output);
			exit();
		}
	}

	// Member deletion confirmed
	function ajax_delete_member()
	{
		if ( ! $this->input->is_ajax_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$member_id = $this->input->post('id');
			$name      = $this->input->post('name');

			if ($this->membership_model->delete_member($member_id, $name))
			{
				$output['result']  = 'success';
				$output['message'] = $this->ion_auth->messages();

				echo json_encode($output);
				exit();
			}
			else
			{
				$output['result']  = 'success';
				$output['message'] = $this->ion_auth->messages();

				echo json_encode($output);
				exit();
			}
		}
	}
}
