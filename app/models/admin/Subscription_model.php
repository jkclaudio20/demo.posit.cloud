<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->local_db = $this->load->database('default', TRUE);
    }

    public function mark_expired()
    {
        $this->local_db->update('settings', ['expired' => 1]);
        return TRUE;
    }

    public function auto_renew()
    {
        $this->cloud_db = $this->load->database('cloud', TRUE);

        $query = $this->cloud_db->limit(1)
                                ->get('settings');

        if ($query->num_rows() > 0)
        {
            $row = $query->row();

            $this->local_db->update('settings', ['expiry_date' => $row->expiry_date, 'expired' => 0]);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function backup_db()
    {
        $query = $this->local_db->where('status', 0)
    							->get('logs');

        $total_count = $query->num_rows();

        $success_count = 0;
        $error_count   = 0;
        $counter       = 0;

        if ($total_count > 0)
        {
            $this->cloud_db = $this->load->database('cloud', TRUE);
            $this->load->model('site_cloud');

            foreach ($query->result() as $row)
    		{
                $counter++;
    			if ($row->transaction == 'add-sale')
    			{
    				// Get Sale Row
    				$query = $this->local_db->where('id', $row->row_id)
    										->limit(1)
    										->get('sales');

    				$sale = $query->row_array();
    				unset($sale['id']);

    				if ($this->cloud_db->insert('sales', $sale))
                    {
                        $new_sale_id = $this->cloud_db->insert_id();

                        // Get Sale Items
                        $query = $this->local_db->where('id', $row->row_id)
                                                ->get('sale_items');

                        $items = $query->result_array();

                        $cost = $this->site_cloud->costing($items);

                        foreach ($items as $item)
                        {
                            $item['sale_id'] = $new_sale_id;
                            unset($item['id']);
                            $this->cloud_db->insert('sale_items', $item);
                        }

                        if ($sale['sale_status'] == 'completed') $this->site_cloud->syncPurchaseItems($cost);

                        // Get Payments
                        $query = $this->local_db->where('sale_id', $row->row_id)
                                                ->get('payments');

                        $payments = $query->result_array();

                        foreach ($payments as $payment)
                        {
                            $payment['sale_id'] = $new_sale_id;
                            unset($payment['id']);
                            $this->cloud_db->insert('payments', $payment);
                            $this->site_cloud->syncSalePayments($new_sale_id);
                        }

                        $this->site_cloud->syncQuantity($new_sale_id);
                        $this->update_award_points($sale['grand_total'], $sale['customer_id'], $sale['created_by']);
                        $this->site_cloud->updateReference('pos');

                        $success_count++;
                        // Update Log
                        $now = new DateTime();
                        $this->local_db->update('logs', array('status' => 1), array('log_id' => $row->log_id));
                        echo "[{$now->format('Y-m-d H:i:s')}] Transaction {$counter} of {$total_count} done.".PHP_EOL;
                    }
                    else
                    {
                        $error_count++;
                        $now = new DateTime();
                        echo "[{$now->format('Y-m-d H:i:s')}] Transaction {$counter} of {$total_count} failed to sync.".PHP_EOL;
                    }
    			}
    		}

            if ($error_count > 0)
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
        else
        {
            // Nothing to backup
            return TRUE;
        }
    }

    public function update_award_points($total, $customer, $user = NULL, $scope = null)
    {
        if (!empty($this->Settings->each_spent) && $total >= $this->Settings->each_spent) {
            $company = $this->site->getCompanyByID($customer);
            $points = floor(($total / $this->Settings->each_spent) * $this->Settings->ca_point);
            $total_points = $scope ? $company->award_points - $points : $company->award_points + $points;
            $this->cloud_db->update('companies', array('award_points' => $total_points), array('id' => $customer));
        }
        if ($user && !empty($this->Settings->each_sale) && !$this->Customer && $total >= $this->Settings->each_sale) {
            $staff = $this->site->getUser($user);
            $points = floor(($total / $this->Settings->each_sale) * $this->Settings->sa_point);
            $total_points = $scope ? $staff->award_points - $points : $staff->award_points + $points;
            $this->cloud_db->update('users', array('award_points' => $total_points), array('id' => $user));
        }
        return true;
    }
}
