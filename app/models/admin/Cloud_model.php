<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cloud_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('cloud', TRUE);
        $this->tables   = $this->config->item('tables', 'cloud');
        $this->backup_tables = $this->config->item('backup_tables', 'cloud');
        $this->all_tables = $this->config->item('all_tables', 'cloud');
        $this->transactions = $this->config->item('transactions', 'cloud');
        $this->cloud_db = $this->load->database('cloud', TRUE);
        $this->local_db = $this->load->database('default', TRUE);
        $this->local_util = $this->load->dbutil($this->local_db, TRUE);
        $this->cloud_util = $this->load->dbutil($this->cloud_db, TRUE);
        $this->mysql_server_path = $this->config->item('mysql_server_path', 'cloud');
        $this->load->model('site_cloud');
        $this->load->model('site');
        $this->load->helper('file');
        $this->load->helper('download');
    }

    /**
     * Sync DB
     *
     * @param  string|boolean $from
     * @param  string|boolean $to
     * @return boolean
     * @author Karlo
     */
    public function sync_db($from = FALSE, $to = FALSE)
    {
    	$from = ($from) ? new DateTime($from) : FALSE;
    	$to   = ($to)   ? new DateTime($to)   : FALSE;

    	$now = new DateTime();

    	if ($from !== FALSE)
    	{
    		$this->local_db->where('datetime >=', $from->format('Y-m-d H:i:s'));

    		if ($to !== FALSE) $this->local_db->where('datetime <=',  $to->format('Y-m-d H:i:s'));
    	}

    	$query = $this->local_db->where('status', 0)
    							->get($this->tables['logs']);

    	$total_count = $query->num_rows();

    	$success_count = 0;

    	if ($total_count > 0)
    	{
    		echo "Local to Cloud DB Synchronization started at {$now->format('Y-m-d H:i:s')}".PHP_EOL;
    		$noun = ($total_count == 1) ? 'transaction' : 'transactions';

    		echo "{$total_count} {$noun} found".PHP_EOL.PHP_EOL;

    		$counter = 0;

    		foreach ($query->result() as $row)
    		{
    			$counter++;
    			$now = new DateTime();
    			echo "[{$now->format('Y-m-d H:i:s')}] Syncing transaction {$counter} of {$total_count}".PHP_EOL;

    			if ($row->transaction == $this->transactions['add_sale'])
    			{
    				// Get Sale Row
    				$query = $this->local_db->where('id', $row->row_id)
    										->limit(1)
    										->get($this->tables['sales']);

    				$sale = $query->row_array();
    				unset($sale['id']);

    				if ($this->cloud_db->insert($this->tables['sales'], $sale))
                    {
                        $new_sale_id = $this->cloud_db->insert_id();

                        // Get Sale Items
                        $query = $this->local_db->where('id', $row->row_id)
                                                ->get($this->tables['sale_items']);

                        $items = $query->result_array();

                        $cost = $this->site_cloud->costing($items);

                        foreach ($items as $item)
                        {
                            $item['sale_id'] = $new_sale_id;
                            unset($item['id']);
                            $this->cloud_db->insert($this->tables['sale_items'], $item);
                        }

                        if ($sale['sale_status'] == 'completed') $this->site_cloud->syncPurchaseItems($cost);

                        // Get Payments
                        $query = $this->local_db->where('sale_id', $row->row_id)
                                                ->get($this->tables['payments']);

                        $payments = $query->result_array();

                        foreach ($payments as $payment)
                        {
                            $payment['sale_id'] = $new_sale_id;
                            unset($payment['id']);
                            $this->cloud_db->insert($this->tables['payments'], $payment);
                            $this->site_cloud->syncSalePayments($new_sale_id);
                        }

                        $this->site_cloud->syncQuantity($new_sale_id);
                        $this->update_award_points($sale['grand_total'], $sale['customer_id'], $sale['created_by']);
                        $this->site_cloud->updateReference('pos');

                        $success_count++;
                        // Update Log
                        $now = new DateTime();
                        $this->local_db->update($this->tables['logs'], array('status' => 1), array('log_id' => $row->log_id));
                        echo "[{$now->format('Y-m-d H:i:s')}] Transaction {$counter} of {$total_count} done.".PHP_EOL;
                    }
                    else
                    {
                        $now = new DateTime();
                        echo "[{$now->format('Y-m-d H:i:s')}] Transaction {$counter} of {$total_count} failed to sync.".PHP_EOL;
                    }
    			}
    		}

    		$now = new DateTime();
    		// Get Logs
    		$query = $this->local_db->get($this->tables['logs']);
    		$logs_data = $query->result_array();

    		$this->cloud_db->empty_table($this->tables['logs']);
    		if ( ! empty($logs_data)) $this->cloud_db->insert_batch($this->tables['logs'], $logs_data);

    		$history = array(
    			'user'     => $this->session->userdata('user_id'),
    			'datetime' => $now->format('Y-m-d H:i:s'),
    			'tx_count' => $success_count
    		);

    		$this->local_db->insert($this->tables['sync_history'], $history);
    		$this->cloud_db->insert($this->tables['sync_history'], $history);

    		echo "[{$now->format('Y-m-d H:i:s')}] Synchronization finished.".PHP_EOL;
    		echo "{$success_count} successful".PHP_EOL;
    		$error_count = $counter - $success_count;
    		echo "{$error_count} failed".PHP_EOL;
            echo "--------------------------------------------------------".PHP_EOL;
            $now = new DateTime();
            echo "[{$now->format('Y-m-d H:i:s')}] Updating local DB.".PHP_EOL;
            echo "This might take some time, please wait...".PHP_EOL;

            // Start Backing up cloud database
            $prefs = array(
                'tables' => $this->backup_tables,
                'format' => 'txt',
                'foreign_key_checks' => FALSE
            );
            $file = $this->backup_db('cloud', $prefs);

            $command = "{$this->mysql_server_path} --user={$this->local_db->username} --password={$this->local_db->password} -h {$this->local_db->hostname} -D {$this->local_db->database} < {$file}";
            shell_exec($command);
            $now = new DateTime();
            echo "[{$now->format('Y-m-d H:i:s')}] Local DB Updated.".PHP_EOL;
    	}
    	else
    	{
    		$now = new DateTime();
    		echo "No data needs to be synchronized. Cloud DB is updated.".PHP_EOL;
    	}
    }

    /**
     * Get Sync Status
     *
     * @author Karlo
     */
    public function get_sync_status()
    {
        // Get last Sync
        $query = $this->local_db->order_by('datetime', 'DESC')
                                ->limit(1)
                                ->get($this->tables['sync_history']);

        if ($query->num_rows() > 0)
        {
            $datetime = new DateTime($query->row()->datetime);
            $now      = new DateTime();

            $diff = ($now->getTimestamp() - $datetime->getTimestamp()) / 60; // in minutes

            if ($diff >= 1440)
            {
                $diff = floor($diff / 1440);
                $unit = ($diff == 1) ? 'day' : 'days';
            }
            elseif ($diff >= 60)
            {
                $diff = floor($diff / 60);
                $unit = ($diff == 1) ? 'hour' : 'hours';
            }
            else
            {
                $diff = floor($diff);
                $unit = ($diff == 1) ? 'minute' : 'minutes';
            }

            echo "Last sync: {$datetime->format('m/d/Y h:i A')} ({$diff} {$unit} ago)".PHP_EOL;
        }
        else
        {
            echo "No sync history found.".PHP_EOL;
        }

        // Get Pending Transactions
        $query = $this->local_db->where('status', 0)
                                ->get($this->tables['logs']);

        $count = $query->num_rows();

        if ($count > 0)
        {
            $noun = ($count == 1) ? 'transaction' : 'transactions';

            echo "{$count} pending {$noun} found.".PHP_EOL;
            echo "Use admin/cloud run command to sync now.".PHP_EOL;
        }
        else
        {
            echo "No pending transactions found.".PHP_EOL;
            echo "DB Synchronization not required.".PHP_EOL;
        }
    }

    /**
     * Test
     */
    public function csv()
    {
        $query = $this->local_db->get('products');

        return $this->local_util->xml_from_result($query);
    }

    // SMA
    public function update_award_points($total, $customer, $user = NULL, $scope = null)
    {
        if (!empty($this->Settings->each_spent) && $total >= $this->Settings->each_spent) {
            $company = $this->site->getCompanyByID($customer);
            $points = floor(($total / $this->Settings->each_spent) * $this->Settings->ca_point);
            $total_points = $scope ? $company->award_points - $points : $company->award_points + $points;
            $this->cloud_db->update('companies', array('award_points' => $total_points), array('id' => $customer));
        }
        if ($user && !empty($this->Settings->each_sale) && !$this->Customer && $total >= $this->Settings->each_sale) {
            $staff = $this->site->getUser($user);
            $points = floor(($total / $this->Settings->each_sale) * $this->Settings->sa_point);
            $total_points = $scope ? $staff->award_points - $points : $staff->award_points + $points;
            $this->cloud_db->update('users', array('award_points' => $total_points), array('id' => $user));
        }
        return true;
    }

    public function deleteBill($id)
    {
        if ($this->cloud_db->delete('suspended_items', array('suspend_id' => $id)) && $this->cloud_db->delete('suspended_bills', array('id' => $id)))
        {
            return true;
        }

        return FALSE;
    }

    public function fix_db()
    {
        // Start Backing up cloud database
        $prefs = array(
            'tables' => $this->all_tables,
            'format' => 'txt',
            'foreign_key_checks' => FALSE
        );
        $file = $this->backup_db('cloud', $prefs);

        $now = new DateTime();

        echo "[{$now->format('Y-m-d H:i:s')}] Importing fetched DB to Local...".PHP_EOL;
        $command = "{$this->mysql_server_path} --user={$this->local_db->username} --password={$this->local_db->password} -h {$this->local_db->hostname} -D {$this->local_db->database} < {$file}";
        shell_exec($command);
        $now = new DateTime();
        echo "[{$now->format('Y-m-d H:i:s')}] Local DB Updated.".PHP_EOL;
    }

    public function backup_db($db = 'local', $prefs = array('format' => 'txt'))
    {
        if ($db == 'local')
        {
            $backup = $this->local_util->backup($prefs);
        }
        else
        {
            $backup = $this->cloud_util->backup($prefs);
        }

        $file = FCPATH.'assets/db_backups/posit_db_'.time().'.sql';
        write_file($file, $backup);

        return $file;
    }

    public function request_sale_discount($sale_id)
    {
        // Get Sale Row
        $query = $this->local_db->where('id', $sale_id)
                                ->limit(1)
                                ->get($this->tables['sales']);

        $sale = $query->row_array();
        unset($sale['id']);
        $sale['discount_status'] = 4;

        $check_q = $this->cloud_db->where('reference_no', $sale['reference_no'])
                                  ->limit(1)
                                  ->get($this->tables['sales']);

        if ($check_q->num_rows() <= 0)
        {
            if ($this->cloud_db->insert($this->tables['sales'], $sale))
            {
                $new_sale_id = $this->cloud_db->insert_id();

                // Get Sale Items
                $query = $this->local_db->where('id', $sale_id)
                                        ->get($this->tables['sale_items']);

                $items = $query->result_array();

                $cost = $this->site_cloud->costing($items);

                foreach ($items as $item)
                {
                    $item['sale_id'] = $new_sale_id;
                    unset($item['id']);
                    $this->cloud_db->insert($this->tables['sale_items'], $item);
                }

                $this->site_cloud->syncQuantity($new_sale_id);
                $this->update_award_points($sale['grand_total'], $sale['customer_id'], $sale['created_by']);

                // Update Log
                $now = new DateTime();
                $this->local_db->update($this->tables['logs'], array('status' => 1), array('row_id' => $sale_id, 'transaction' => 'add-sale'));

                return TRUE;
            }
            else
            {
                $this->ion_auth->set_error('Sync failed. Check your internet connection and try again.');
                return FALSE;
            }
        }

        return TRUE;
    }

    public function fetch_discount_status($reference_no)
    {
        $query = $this->cloud_db->where('reference_no', $reference_no)
                                ->limit(1)
                                ->get('sales');

        if ($query->num_rows() > 0)
        {
            return $query->row()->discount_status;
        }
        else
        {
            $this->ion_auth->set_error('Sale does not exist in cloud.');
            return FALSE;
        }
    }
}
