ALTER TABLE `posit_demo`.`sma_settings` ADD COLUMN `expiry_date` DATE NULL AFTER `pdf_lib`;
ALTER TABLE `posit_demo`.`sma_settings` ADD COLUMN `expired` TINYINT(1) DEFAULT 0 NOT NULL AFTER `expiry_date`;
ALTER TABLE `posit_demo`.`sma_settings` DROP COLUMN `version`;