###################
What is POSit Cloud
###################

POSit Cloud is a reliable Sales and Inventory System developed by Trendmedia Inc.
Simplify the whole process of your business in one integrated system.

*******************
Release Information
*******************

This repo contains in-development code for future releases.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

Trendmedia Inc. © 2020
